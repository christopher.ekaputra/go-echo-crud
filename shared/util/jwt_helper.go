package util

import (
	"crud_echo/config"
	"crud_echo/pkg/domain"
	"time"

	"github.com/golang-jwt/jwt"
)

type JwtClaims struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	jwt.StandardClaims
}

func CreateJwtToken(user domain.User) (string, error) {
	conf := config.GetConfig()
	claims := JwtClaims{
		Id:       user.Id,
		Username: user.Username,
		Email:    user.Email,
		Address:  user.Address,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
		},
	}
	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	token, err := rawToken.SignedString([]byte(conf.SignKey))
	if err != nil {
		return "", err
	}
	return token, nil
}
