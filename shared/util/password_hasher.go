package util

import (
	"encoding/base64"

	"golang.org/x/crypto/bcrypt"
)

// To be used in the future
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// To be used in the future
func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func EncryptPassword(password string) string {
	return base64.StdEncoding.EncodeToString([]byte(password))
}

func DecryptPassword(password string) string {
	data, err := base64.StdEncoding.DecodeString(password)
	if err != nil {
		panic(err)
	}
	return string(data)
}
