CREATE TABLE files(
    id BIGSERIAL PRIMARY KEY,
    link VARCHAR(1000) NOT NULL
);