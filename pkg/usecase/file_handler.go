package usecase

import (
	"context"
	"crud_echo/config"
	"crud_echo/pkg/domain"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"strings"

	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
)

type FileHandlerUsecase struct {
	Firebase              *firebase.App
	BucketHandle          *storage.BucketHandle
	FileHandlerRepository domain.FileHandlerRepository
}

func NewFileHandlerUsecase(fileHandlerRepository domain.FileHandlerRepository) domain.FileHandlerUsecase {
	opt := option.WithCredentialsFile("config/firebase-config.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		panic(err)
	}
	client, err := app.Storage(context.Background())
	if err != nil {
		panic(err)
	}

	conf := config.GetConfig()
	// Get a storage reference
	storeRef, err := client.Bucket(conf.FirebaseBucketName)
	if err != nil {
		panic(err)
	}
	return &FileHandlerUsecase{
		Firebase:              app,
		BucketHandle:          storeRef,
		FileHandlerRepository: fileHandlerRepository,
	}
}

func (fhu FileHandlerUsecase) UploadFile(file *multipart.FileHeader) error {
	var fileHandler domain.FileHandler

	src, err := file.Open()
	if err != nil {
		return errors.New("error in opening file")
	}
	defer src.Close()

	// Destination
	dst := fmt.Sprintf("go_rest_api/%s", file.Filename)

	fileHandler.Link = dst
	err = fhu.UploadToFirebase(dst, src)
	if err != nil {
		return err
	}
	filteredString := strings.ReplaceAll(file.Filename, " ", "%20")
	firebaseLink := fmt.Sprintf("https://firebasestorage.googleapis.com/v0/b/go-rest-api-bc365.appspot.com/o/go_rest_api%%2F%s?alt=media", filteredString)
	fileHandler.Link = firebaseLink
	return fhu.FileHandlerRepository.UploadFile(fileHandler)

}

func (fhu FileHandlerUsecase) UploadToFirebase(dst string, src multipart.File) error {

	// Create a storage writer
	wc := fhu.BucketHandle.Object(dst).NewWriter(context.Background())

	// Copy the file to the storage writer
	if _, err := io.Copy(wc, src); err != nil {
		return err
	}
	if err := wc.Close(); err != nil {
		return err
	}
	return nil

}

func (fhu FileHandlerUsecase) DownloadFile(id int) (interface{}, error) {
	resp, err := fhu.FileHandlerRepository.DownloadFile(id)
	if err != nil {
		return nil, errors.New("id file not found")
	}

	return resp, err
}
