package usecase

import (
	"crud_echo/pkg/domain"
	"crud_echo/pkg/dto"
	"crud_echo/shared/util"
	"errors"

	"github.com/mitchellh/mapstructure"
)

type UserUsecase struct {
	UserRepository domain.UserRepository
}

func NewUserUsecase(UserRepository domain.UserRepository) domain.UserUsecase {
	return &UserUsecase{
		UserRepository: UserRepository,
	}
}

func (su UserUsecase) GetUsers() ([]domain.User, error) {
	return su.UserRepository.GetUsers()
}

func (su UserUsecase) CreateUser(req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	if _, err := su.UserRepository.GetUserByEmail(user.Email); err == nil {
		return errors.New("Email already exist")
	}
	// Hash password
	user.Password = util.EncryptPassword(user.Password)
	return su.UserRepository.CreateUser(user)
}

func (su UserUsecase) GetUser(id int) (domain.User, error) {
	return su.UserRepository.GetUser(id)
}

func (su UserUsecase) UpdateUser(id int, req dto.UserDTO) error {
	var user domain.User
	mapstructure.Decode(req, &user)
	// Hash password
	user.Password, _ = util.HashPassword(user.Password)
	return su.UserRepository.UpdateUser(id, user)
}

func (su UserUsecase) DeleteUser(id int) error {
	return su.UserRepository.DeleteUser(id)
}

func (su UserUsecase) GetUserByEmail(email string) (domain.User, error) {
	return su.UserRepository.GetUserByEmail(email)
}

func (su UserUsecase) Login(req dto.LoginRequest) (interface{}, error) {
	var loginResponse dto.LoginResponse
	user, err := su.UserRepository.GetUserByEmail(req.Email)
	if err != nil {
		return nil, errors.New("email not found")
	}
	passwordValid := util.DecryptPassword(user.Password)
	if passwordValid != req.Password {
		return nil, errors.New("bad credential")
	}
	token, err := util.CreateJwtToken(user)
	if err != nil {
		return nil, err
	}
	mapstructure.Decode(user, &loginResponse)
	loginResponse.Token = token

	return loginResponse, err

}
