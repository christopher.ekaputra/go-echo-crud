package dto

import validation "github.com/go-ozzo/ozzo-validation"

type UserDTO struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Password string `json:"password"`
}

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Token    string `json:"token"`
}

func (s UserDTO) Validation() error {
	err := validation.ValidateStruct(&s,
		validation.Field(&s.Username, validation.Required),
		validation.Field(&s.Email, validation.Required),
		validation.Field(&s.Password, validation.Required))

	if err != nil {
		return err
	}
	return nil
}
