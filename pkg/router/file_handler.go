package router

import (
	"crud_echo/pkg/controller"
	"crud_echo/pkg/repository"
	"crud_echo/pkg/usecase"
	"database/sql"

	"github.com/labstack/echo/v4"
)

func NewFileHandlerRouter(e *echo.Echo, g *echo.Group, db *sql.DB) {
	fhr := repository.NewFileHandlerRepository(db)
	fhu := usecase.NewFileHandlerUsecase(fhr)
	fhc := &controller.FileHandlerController{
		FileHandlerUsecase: fhu,
	}

	e.POST("/UploadFile", fhc.UploadFile)
	e.GET("/DownloadFile/:id", fhc.DownloadFile)

}
