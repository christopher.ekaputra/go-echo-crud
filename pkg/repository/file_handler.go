package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type FileHandlerRepository struct {
	db *sql.DB // nil
}

func NewFileHandlerRepository(db *sql.DB) domain.FileHandlerRepository {
	return &FileHandlerRepository{
		db: db,
	}
}

func (fhr FileHandlerRepository) UploadFile(file domain.FileHandler) error {
	sql := `INSERT INTO files (link) values ($1)`
	stmt, err := fhr.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(file.Link)
	if err2 != nil {
		return err2
	}
	return nil
}

func (fhr FileHandlerRepository) DownloadFile(id int) (domain.FileHandler, error) {
	var file domain.FileHandler
	sql := `SELECT * FROM files WHERE id = $1`

	err := fhr.db.QueryRow(sql, id).Scan(&file.Id, &file.Link)
	return file, err
}
