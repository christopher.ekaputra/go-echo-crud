package domain

import (
	"mime/multipart"
)

type FileHandler struct {
	Id   int    `json:"id"`
	Link string `json:"link"`
}

type FileHandlerRepository interface {
	UploadFile(file FileHandler) error
	DownloadFile(id int) (FileHandler, error)
}

type FileHandlerUsecase interface {
	UploadFile(file *multipart.FileHeader) error
	DownloadFile(id int) (interface{}, error)
}
