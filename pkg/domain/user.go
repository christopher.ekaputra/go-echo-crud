package domain

import "crud_echo/pkg/dto"

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Address  string `json:"address"`
	Password string `json:"password"`
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req User) error
	UpdateUser(id int, req User) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req dto.UserDTO) error
	UpdateUser(id int, req dto.UserDTO) error
	DeleteUser(id int) error
	GetUserByEmail(email string) (User, error)
	Login(req dto.LoginRequest) (interface{}, error)
}
