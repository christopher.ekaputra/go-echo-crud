package controller

import (
	"crud_echo/pkg/domain"
	"crud_echo/shared/response"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
)

type FileHandlerController struct {
	FileHandlerUsecase domain.FileHandlerUsecase
}

func (fhc *FileHandlerController) UploadFile(c echo.Context) error {

	// Source
	file, err := c.FormFile("file")
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	err = fhc.FileHandlerUsecase.UploadFile(file)
	if err != nil {
		return response.SetResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	return response.SetResponse(c, http.StatusOK, "success", nil)
}

func (fhc *FileHandlerController) DownloadFile(c echo.Context) error {

	id, _ := strconv.Atoi(c.Param("id"))
	resp, err := fhc.FileHandlerUsecase.DownloadFile(id)
	if err != nil {
		return response.SetResponse(c, http.StatusNotFound, err.Error(), nil)
	}
	return response.SetResponse(c, http.StatusOK, "success", resp)
}
